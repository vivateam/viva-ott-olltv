# -*- coding: utf-8 -*-

import warnings
import json
import io
import os
import os.path
import sys
import requests
import logging
import time
import ssl
import hashlib

from datetime import datetime, timedelta
from requests.auth import HTTPBasicAuth
from config import *



def viva(logger, data):

  try:
    auth = HTTPBasicAuth(VIVA_USERNAME, VIVA_PASSWORD)
    r = requests.post(VIVA_URL, json=data, auth=auth, verify=VIVA_CERT, headers=VIVA_HEADERS)
    if r.json:
      return {'result':'success','data': r.json()}
    else:
      logger.info('Error Viva')
      return {'result':'error','data':'Data is not JSON format'}
  except Exception as e:
    logger.info('Error Viva: %s' % (e))
    return {'result':'error'}



def auth2(logger):

  try:
    ufunc = '%s/auth2/' % (url)
    payload = {
      'password': password,
      'login': login
      }
    r = requests.post(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      #logger.info(r.json())
      return r.json()
    else:
      logger.info('Error auth2: Invalid JSON')
      return {'hash': None, 'status': 1000}

  except Exception as e:
    logger.info('Error auth2: %s' % (e))
    return {'hash': None, 'status': 1001}





def account_exists(logger,hsh,jdata):

  try:
    ufunc = '%s/accountExists?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error getUserInfo')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error getUserInfo')
    return {'hash': None, 'status': 1001}




def add_user(logger,hsh,jdata):

  try:
    ufunc = '%s/addUser?hash=%s' % (url,hsh)
    payload = {
      'email': jdata['email'],
      'account': jdata['account'],
      'password': jdata['password'],
      'phone': jdata['phone'],
      'receive_news': jdata['receive_news'],
      'send_registration_email': jdata['send_registration_email']
      }

    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.post(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error addUser')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error addUser')
    return {'hash': None, 'status': 1001}



def change_account(logger,hsh,jdata):

  try:
    ufunc = '%s/changeAccount?hash=%s' % (url,hsh)
    payload = {
      'email': jdata['email'],
      'account': jdata['account']
      }

    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.post(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error changeAccount')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error changeAccount')
    return {'hash': None, 'status': 1001}



def delete_account(logger,hsh,jdata):

  try:
    ufunc = '%s/deleteAccount?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account']
      }

    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.post(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error deleteAccount')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error deleteAccount')
    return {'hash': None, 'status': 1001}



def get_user_list(logger,hsh,jdata):

  try:
    ufunc = '%s/getUserList?hash=%s' % (url,hsh)
    payload = {
      'offset': jdata['offset'],
      'limit': jdata['limit']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      return r.json()
    else:
      logger.info('Error getUserList')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error getUserList')
    return {'hash': None, 'status': 1001}





def get_user_info(logger,hsh,jdata):

  try:
    ufunc = '%s/getUserInfo?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error getUserInfo')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error getUserInfo')
    return {'hash': None, 'status': 1001}



def change_password(logger,hsh,jdata):

  try:
    ufunc = '%s/changeUserInfo?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account'],
      'password': jdata['password']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error changeUserInfo')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error changeUserInfo')
    return {'hash': None, 'status': 1001}



def enable_bundle(logger,hsh,jdata):

  try:
    ufunc = '%s/enableBundle?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account'],
      'sub_id': jdata['sub_id'],
      'type': jdata['type']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      data = r.json()
      code = data['data']
      if int(code)>0:
        command = {
          'action': 'activation_code',
          'params': {
          'code': code,
          'login': jdata['account']
          }
        }
        result = viva(logger, command)

      return r.json()
    else:
      logger.info('Error enableBundle. Answer data isnt JSON')
      return {'hash': None, 'status': 1000}

  except Exception as e:
    logger.info('Error enableBundle: %s' % (e))
    return {'hash': None, 'status': 1001}



def disable_bundle(logger,hsh,jdata):

  try:
    ufunc = '%s/disableBundle?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account'],
      'sub_id': jdata['sub_id'],
      'type': jdata['type']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error disableBundle')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error disableBundle')
    return {'hash': None, 'status': 1001}



def get_user_devices(logger,hsh,jdata):

  try:
    ufunc = '%s/getUserDevices?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error getUserInfo')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error getUserInfo')
    return {'hash': None, 'status': 1001}




def reset_parent_control(logger,hsh,jdata):
  try:
    ufunc = '%s/resetParentControl?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error getUserInfo')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error getUserInfo')
    return {'hash': None, 'status': 1001}




def delete_device(logger,hsh,jdata):

  try:
    ufunc = '%s/delDevice?hash=%s' % (url,hsh)
    payload = {
      'account': jdata['account'],
      'mac': jdata['mac'],
      'serial_number': jdata['serial_number'],
      'type': jdata['type']
      }
    logger.info('func=%s payload=%s' % (ufunc, payload))

    r = requests.get(ufunc, params=payload, headers=headers, verify=False) 

    if r.json:
      logger.info(r.json())
      return r.json()
    else:
      logger.info('Error getUserInfo')
      return {'hash': None, 'status': 1000}

  except:
    logger.info('Error getUserInfo')
    return {'hash': None, 'status': 1001}