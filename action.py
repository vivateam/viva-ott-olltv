# encoding: utf-8

import os
import json
import logging
import csv
import glob
import sys
import requests
import warnings
import signal
import time
import funcs

from datetime import date, timedelta, datetime, timezone
from logging.handlers import RotatingFileHandler
from requests.auth import HTTPBasicAuth

from config import *

LOG_FILE = '/var/log/%s.log' % (ALIAS)

warnings.filterwarnings("ignore")

RUN = True

def handler_stop_signals(signum, frame):
  global RUN
  RUN = False

signal.signal(signal.SIGINT, handler_stop_signals)
signal.signal(signal.SIGTERM, handler_stop_signals)



def action():

  logger = logging.getLogger(ALIAS)
  formatter = logging.Formatter('%(asctime)s - %(name)s - %(message)s')
  logger.setLevel(logging.INFO)
  handler = RotatingFileHandler(LOG_FILE, maxBytes=200000, backupCount=5)
  handler.setFormatter(formatter)
  logger.addHandler(handler)

  pid = "%s" % (os.getpid())

  logger.info("Start process with Pid %s" % (pid))

  while RUN:

    try:

      if RUN == False:
        logger.info('Stop process with Pid %s' % (pid))
        quit()

      command = {
        'action':'list',
        'params': {'cod_iptv':COD}
        }

      result = funcs.viva(logger, command)

      sorted_obj = dict(result) 
      sorted_obj['data'] = sorted(result['data'], key=lambda x : x['id'], reverse=False)
      arr_id_success = []
      arr_id_error = []

      for row in sorted_obj['data']:
        id = row['id']
        command = row['data']
        data = command['data']
        action = command['action']

        logger.info('query:%s data:%s' % (action, data))


        # add user
        if command['action'] == 'add_user':

          auth2 = funcs.auth2(logger)

          code = 0

          phone = data['public_phone']
          account = data['login']
          email = data['mail']
          password = data['password']
          status = data['status']
          subs = data['subs']

          # check if account exists
          jdata = {'account': account}
          account_exists = funcs.account_exists(logger, auth2['hash'], jdata)

          if account_exists['status'] == 0:
            if account_exists['data'] == 0:

              jdata = {
                'email': email,
                'account': account,
                'password': password,
                'phone': phone,
                'receive_news': 0,
                'send_registration_email': 0
                }

              add_user = funcs.add_user(logger, auth2['hash'], jdata)
              if add_user['status'] == 0:
                code = int(add_user['data'])

            else:

              code = int(account_exists['data']['ID'])
              logger.info('Account %s is exists with code %s' % (account, code))

              # disable all subscribes
              for subscribe in account_exists['data']['bought_subs']:
                jdata = {
                  'account': account,
                  'sub_id': subscribe['sub_id'],
                  'type': subscribe['activation_type']
                  }
                disable_bundle = funcs.disable_bundle(logger, auth2['hash'], jdata)

          # enable all subscribes
          if code > 0:
            arr_id_success.append(id)
            if status == 1:
              for subscribe in subs:
                jdata = {
                  'account':account,
                  'sub_id':subscribe,
                  'type':'subs_no_device'
                  }
                enable_bundle = funcs.enable_bundle(logger, auth2['hash'], jdata)
          else:
            arr_id_error.append(id)


        # change status
        if command['action'] == 'change_status':

          auth2 = funcs.auth2(logger)
          account = data['login']
          status = data['status']
          subs = data['subs']

          for subscribe in subs:

            if status == 1:
              jdata = {
                'account': account,
                'sub_id': subscribe,
                #'type': 'subs_no_device'
                'type': 'subs_renew' 
                }
              enable_bundle = funcs.enable_bundle(logger, auth2['hash'], jdata)
              arr_id_success.append(id)
            else:
              jdata = {
                'account': account,
                'sub_id': subscribe,
                'type': 'subs_negative_balance'
                }
              disable_bundle = funcs.disable_bundle(logger, auth2['hash'], jdata)
              arr_id_success.append(id)


        # subscribe
        if command['action'] == 'subscribe':
          status = data['status']
          if status == 1:
            auth2 = funcs.auth2(logger)
            account = data['login']
            subscribe = data['subscribe']
            jdata = {
              #'login': login,
              'account':account,
              'sub_id':subscribe,
              #'type':'subs_no_device'
              'type': 'subs_renew'
              }
            enable_bundle = funcs.enable_bundle(logger, auth2['hash'], jdata)
          arr_id_success.append(id)


        # unsubscribe
        if command['action'] == 'unsubscribe': 
          status = data['status']
          if status == 1:
            auth2 = funcs.auth2(logger)
            account = data['login']
            subscribe = data['subscribe']
            jdata = {
              'account':account,
              'sub_id':subscribe,
              'type':'subs_negative_balance'
              }
            disable_bundle = funcs.disable_bundle(logger, auth2['hash'], jdata)
          arr_id_success.append(id)


        # delete user
        if command['action'] == 'delete_user': 
          status = data['status']
          if status == 1:
            auth2 = funcs.auth2(logger)
            account = data['login']
            subs = data['subs']
            for subscribe in subs:
              jdata = {
                'account':account,
                'sub_id':subscribe,
                'type':'subs_break_contract'
                }
              disable_bundle = funcs.disable_bundle(logger, auth2['hash'], jdata)
          arr_id_success.append(id)


        # change password
        if command['action'] == 'change_password':
          auth2 = funcs.auth2(logger)
          account = data['login']
          password = data['password']
          jdata = {
            'account': account,
            'password': password
            }
          change_password = funcs.change_password(logger, auth2['hash'], jdata)
          arr_id_success.append(id)


        # change_packet
        if command['action'] == 'change_packet':
          arr_id_success.append(id)


        # get User Devices
        if command['action'] == 'get_user_devices': 
          auth2 = funcs.auth2(logger)
          account = data['login']
          jdata = {
            'account': account
            }
          get_user_devices = funcs.get_user_devices(logger, auth2['hash'], jdata)
          command = {
            'action':'get_user_devices',
            'params': {
            'login': data['login'],
            'device_list': get_user_devices
            }
            }
          result = funcs.viva(logger, command)
          arr_id_success.append(id)


        # delete device
        if command['action'] == 'delete_device':
          auth2 = funcs.auth2(logger)
          account = data['login']
          mac = data['mac']
          serial_number = data['serial']
          jdata = {
            'account': account,
            'mac': mac,
            'serial_number': serial_number,
            'type':'device_break_contract'
            }
          delete_device = funcs.delete_device(logger, auth2['hash'], jdata)
          arr_id_success.append(id)



        # reset parent control
        if command['action'] == 'reset_parent_control': 
          auth2 = funcs.auth2(logger)
          account = data['login']
          jdata = {
            'account': account
            }
          reset_parent_control = funcs.reset_parent_control(logger, auth2['hash'], jdata)
          arr_id_success.append(id)


        # flush_user
        if command['action'] == 'flush_user': 
          arr_id_success.append(id)


        # finish

      cou_id_success = len(arr_id_success)
      cou_id_error = len(arr_id_error)
      cou_all = cou_id_success + cou_id_error

      if cou_id_success > 0:
        logger.info('arr_id_success: %s' % (arr_id_success))

      if cou_id_error > 0:
        logger.info('arr_id_error: %s' % (arr_id_error))

      if cou_all > 0:
        command = {
          'action':'set',
          'params': {'success': arr_id_success, 'error': arr_id_error}
          }
        result = funcs.viva(logger, command)


    except Exception as e:
      logger.error('ERROR: %s' % (e))

    time.sleep(5)


  logger.info('Stop process with Pid %s' % (pid))


if __name__ == "__main__":

  action()
